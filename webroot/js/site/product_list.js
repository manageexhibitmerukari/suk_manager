////////////////////////////////////////////////////      ユーザー管理       /////////////////////////////////////////////

var CONTEXT_NAME = "user-products";
var tbl_data = [];
var tbl_data_pos = -1;
var refresh_list = false;
var currentpage = 0;
var firstload = true;
var currentscrollYpos = 0;
var currentscrollXpos = 0;
var allchecked = false;
var selectproductary_itemid= [];
var main_Table;



$(document).ready(function()
{
    make_init_data();

    $('#product_add_modal').on('hidden.bs.modal', function() {
        $('#product_add_form').formValidation('resetForm', true);
        if(refresh_list)
            make_init_data();
    });

    $('#product_add_modal').on('shown.bs.modal', function() {
        refresh_list = false;
    });

    $('#product_edit_modal').on('hidden.bs.modal', function() {
        $('#product_edit_form').formValidation('resetForm', true);
        if(refresh_list)
            make_init_data();
    });

    $('#product_edit_modal').on('shown.bs.modal', function() {
        refresh_list = false;
    });

    $('#csv_import_modal').on('hidden.bs.modal', function() {
        $('#csv_import_form').formValidation('resetForm', true);
    });

    $("#save_product").click(function () {

        bootbox.setDefaults({locale:"ja"});
        bootbox.confirm( {
            message: "変更を保存しますか？",
            callback:  function(result) {
                if(result == true)
                {
                    var fields = $(":input").serializeArray();

                    // call back ajax function
                    $.ajax({
                        type: "POST",
                        url: "/user-products/list-update",
                        data: {formobjects: encodeURIComponent(JSON.stringify(fields))},
                        contentType: "application/x-www-form-urlencoded;",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                            success_add_flag = 0;
                            $("#loading_overlay").fadeIn(300);
                        },
                        error: function (data, status, errThrown) {
                            $("#loading_overlay").fadeOut(300);
                        },
                        success: function (data) {
                            if (data.result == "success") {
                                success_add_flag = 0;
                                firstload = true;
                                make_init_data();
                            } else if (data.result == "error") {
                                success_add_flag = 0;
                                firstload = true;
                                make_init_data();
                                alert(data.msg);
                            }
                        },
                        statusCode: {
                            404: function () {
                                success_add_flag = 0;
                                alert('page not found');
                                $("#loading_overlay").fadeOut(300);
                            }
                        }
                    });
                }
            }
        });
    });

    $('#csv_import_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            csv_file: {
                validators: {
                    notEmpty: {
                        message: 'csvファイルを入力してください。'
                    },
                    file: {
                        extension: 'csv',
                        type: 'application/vnd.ms-excel, text/csv',
                        //maxSize: 2097152,   // 2048 * 1024
                        message: 'csvファイル形式はありません。'
                    }
                }
            }
        }
    }).on('success.form.fv', function(e) {
        // Retrieve instances
        //var $form = $(e.target);        // The form instance
        //    fv    = $(e.target).data('formValidation'); // FormValidation instance

        // check the submit request from input window or preview window
        $("#loading_overlay").show();
    });


    $('#product_add_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            suk: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: '最大アカウント数必須です。'
                    }
                }
            },
            note: {
                validators: {
                    notEmpty: {
                        message: '最大アカウント数必須です。'
                    }
                }
            },
            remain_amount: {
                validators: {
                    notEmpty: {
                        message: '最大アカウント数必須です。'
                    }
                }
            }
        }
    }).on('success.form.fv', function(e) {
        // Prevent form submission

        e.preventDefault();

        // Retrieve instances
        var $form = $(e.target),        // The form instance
            fv    = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request

        if (addProduct($form.serialize())) {
            $('#product_add_modal').modal('hide');
            refresh_list = true;
        }
    });

    $('#product_edit_form').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err: {
            // You can set it to popover
            // The message then will be shown in Bootstrap popover
            container: 'tooltip'
        },
        fields: {
            suk: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: '最大アカウント数必須です。'
                    }
                }
            },
            note: {
                validators: {
                    notEmpty: {
                        message: '最大アカウント数必須です。'
                    }
                }
            },
            remain_amount: {
                validators: {
                    notEmpty: {
                        message: '最大アカウント数必須です。'
                    }
                }
            }
        }
    }).on('success.form.fv', function(e) {
        // Prevent form submission

        e.preventDefault();

        // Retrieve instances
        var $form = $(e.target),        // The form instance
            fv    = $(e.target).data('formValidation'); // FormValidation instance

        // Do whatever you want here ... ajax request

        if (editProduct($form.serialize())) {
            $('#product_edit_modal').modal('hide');
            refresh_list = true;
        }
    });
});


function make_init_data(){
    // call back ajax function
    if($("#loading_overlay").not(":visible"))
        $("#loading_overlay").fadeIn(300);

    $.ajax({
        type: "POST",
        url: "/products/index",
        contentType: "application/x-www-form-urlencoded;",
        dataType: "json",
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
        },
        success: function(data){
            init_tbl_data(data.products);
            make_list(tbl_data);

            $("#loading_overlay").fadeOut(300);
        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay").hide();
            }
        }
    });
}

function init_tbl_data(input_data){
    tbl_data = input_data;
}

function checkInput(obj){
    start = obj.selectionStart;
    end = obj.selectionEnd;
    obj.value = obj.value.replace(/[^0-9.]/g, '');
    obj.value = obj.value.replace(/(\..*)\./g, '$1');
    obj.selectionStart = start;
    obj.selectionEnd = end;
}

$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings){
    return {
        "iPage": Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};



function make_list(input_data)
{
    var main_tbl_wrap = $('#main_tbl_wrap');  //font tag - most wrapper tag
    var inner_html = '';
    inner_html += '<table id="main_tbl" class="table table-hover">';
    if(input_data.length <= 0){
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th align = "center" style="min-width:20px !important;"></th>';
        inner_html += '<th style="text-align: center; padding-right: 0">管理番号</th>';
        inner_html += '<th style="text-align: center; padding-right: 0">在庫</th>';
        inner_html += '<th style="text-align: center; padding-right: 0">価格</th>';
        inner_html += '<th style="text-align: center; padding-right: 0">備考</th>';
        inner_html += '<th style="text-align: center; padding-right: 0">作用</th>';

        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        inner_html += '<tr>';
        inner_html += '<td style="text-align:center;vertical-align:middle;padding-top:20px;" colspan=5>';
        inner_html += '<div class="alert alert-warning alert-dismissable">';
        inner_html += 'データはありません。';
        inner_html += '</div>';
        inner_html += '</td>';
        inner_html += '</tr>';
        inner_html += '</tbody>';
    }
    else{
        inner_html += '<thead>';
        inner_html += '<tr>';
        inner_html += '<th align = "center" style="width:10px !important;padding-right: 0px;"><input type =checkbox id="flowcheckall" onchange="checkAll(this)" '+((allchecked)?'checked':'')+'></th>';
        inner_html += '<th style="text-align: center; padding-right: 0">管理番号</th>';
        inner_html += '<th style="text-align: center; padding-right: 0">在庫</th>';
        inner_html += '<th style="text-align: center; padding-right: 0">価格</th>';
        inner_html += '<th style="text-align: center; padding-right: 0">備考</th>';
        inner_html += '<th style="text-align: center; width: 80px; padding-right: 0">作用</th>';
        inner_html += '</tr>';
        inner_html += '</thead>';
        inner_html += '<tbody>';
        for(i=0; i<input_data.length; i++) {

            inner_html += '<tr id="' + input_data[i].id + '">';

            for (key in input_data[i]) {
                if (input_data[i][key] == null) input_data[i][key] = "";
            }

            input_data[i].No = (input_data.length - i);

            inner_html += '<td align = "center" style="vertical-align: top;">';
            inner_html += '<input onchange = "selectproduct(this)" type = checkbox id="check' + input_data[i]['id'] + '" >';
            inner_html += '</td>';

            // SUK
            inner_html += '<td style="text-align: center;vertical-align:top">' ;
            inner_html += input_data[i]['suk'];
            inner_html += '</td>';

            // quantity
            inner_html += '<td style="text-align: center;vertical-align:top">' ;
            inner_html += input_data[i]['remain_amount'];
            inner_html += '</td>';

            // price
            inner_html += '<td style="text-align: center;vertical-align:top">' ;
            inner_html += input_data[i]['price'];
            inner_html += '</td>';

            // note
            inner_html += '<td style="text-align: center;vertical-align:top">' ;
            inner_html += input_data[i]['note'];
            inner_html += '</td>';

            //edit button
            inner_html +=  '<td>' ;
            inner_html += '<button onclick="displayUpdateUI(' + input_data[i]['id'] + ');" id="' + input_data[i]['id'] + '" class="btn btn-primary btn-sm" style="display:block; margin-bottom:5px"><i class="fa fa-edit"></i>&nbsp;更新</button>';
            inner_html +=  '</td>';

            inner_html += '</tr>';
        }
        inner_html += '</tbody>';
    }
    inner_html += '</table>';
    main_tbl_wrap.html(inner_html);
    if(input_data.length > 0)
    {
        if(typeof(Cookies.get('idisplaylength')) == "undefined" && Cookies.get('idisplaylength') === null)
            idisplaylength = 5;
        else
            idisplaylength = Cookies.get('idisplaylength');

        main_Table = $('#main_tbl').dataTable({
            "initComplete": function (oSettings) { //changed line
                if ( this.fnPagingInfo().iTotalPages < currentpage + 1)
                    currentpage = currentpage -1;

                //this.fnPageChange( currentpage );
                firstload = false;
            },
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "bAutoWidth": false,
            "iDisplayLength": parseInt(idisplaylength, 10),
            "aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "すべて"]],
            "language": {
                "url": "/css/plugins/datatables/dt_i18n/Japanese.lang"
            },
            "fnDrawCallback": function()
            {
                var checkboxes = document.getElementsByTagName('input');
                var checkall =  document.getElementById("flowcheckall");

                if (checkall.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            item_id = checkboxes[i].id.replace('check','');
                            checkboxes[i].checked = (selectproductary_itemid[item_id] == 1);
                        }
                    }
                }

                if (!firstload)
                {
                    currentpage = this.fnPagingInfo().iPage;
                }

                $("#main_tbl thead th:first").removeClass("sorting_desc").removeClass("sorting_asc").addClass("sorting_disabled");
            }
        });

        $('#main_tbl').on( 'length.dt', function ( e, settings, len ) {
            Cookies.set('idisplaylength', len);
        } );
    }

    if($("#loading_overlay").is(":visible"))
        $("#loading_overlay").hide();
}

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function () {
    function extend () {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[ i ];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }
    function init (converter) {
        function api (key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path    && '; path=' + attributes.path,
                    attributes.domain  && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }
    return init();
}));

function checkAll(ele) {
    for (index in tbl_data) {
        item_id = tbl_data[index]['id'];
        selectproductary_itemid[item_id] = ele.checked ? 1: 0;
    }
    allchecked = ele.checked;
    main_Table.fnStandingRedraw();
}

function selectproduct(obj)
{
    item_id = obj.id.replace('check','');

    for (index in tbl_data) {
        itemid = tbl_data[index]['id'];

        if(item_id == itemid)
        {
            selectproductary_itemid[itemid] = ( obj.checked )?1:0;
        }
    }

    allchecked = false;
    document.getElementById('flowcheckall').checked = false;
}

$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
    //redraw to account for filtering and sorting
    // concept here is that (for client side) there is a row got inserted at the end (for an add)
    // or when a record was modified it could be in the middle of the table
    // that is probably not supposed to be there - due to filtering / sorting
    // so we need to re process filtering and sorting
    // BUT - if it is server side - then this should be handled by the server - so skip this step
    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }

    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};

function RemoveCheckedProduct(){
    productary = Array();
    for (index in selectproductary_itemid){
        value = selectproductary_itemid[index];
        if (value == 1)
        {
            productary.push(index);
        }
    }

    if (productary.length <= 0)
    {
        bootbox.alert("ご選択された商品がありません。");
        return;
    }


    bootbox.setDefaults({locale:"ja"});
    bootbox.confirm( {
        message:"本当に削除しますか？",
        callback:  function(result) {
            if(result == true)
            {
                $("#loading_overlay").show();
                $.ajax({
                    type: "POST",
                    url: "/products/removeCheckedProducts",
                    data: "product_id_array="+JSON.stringify(productary),
                    contentType: "application/x-www-form-urlencoded;",
                    dataType: "json",
                    async: true,
                    beforeSend: function() {
                        $("#loading_overlay").fadeIn(300);
                    },
                    error: function(data, status, errThrown){
                        $("#loading_overlay").fadeOut(300);
                    },
                    success: function(data){
                        if(data.result == "success"){
                            selectproductary = Array();
                            selectproductary_itemid = Array();
                            make_init_data();

                        }else if(data.result == "error"){
                            $("#loading_overlay").fadeOut(300);
                        }
                    },
                    statusCode: {
                        404: function() {
                            alert('page not found');
                            $("#loading_overlay").fadeOut(300);
                        }
                    }
                });
            }
        }
    });
}

function displayInsertUI(){
    $('#product_add_form #suk').val('');
    $('#product_add_form #note').val('');
    $('#product_add_form #price').val('');
    $('#product_add_form #remain_amount').val('');

    $('#product_add_modal').modal();
}

function displayUpdateUI(product_id){
    tbl_data_pos=tbl_data.map(function (element) { return element.id;}).indexOf(product_id);

    $('#product_edit_form #suk_edit').val(tbl_data[tbl_data_pos].suk);
    $('#product_edit_form #note_edit').val(tbl_data[tbl_data_pos].note);
    $('#product_edit_form #price_edit').val(tbl_data[tbl_data_pos].price);
    $('#product_edit_form #remain_amount_edit').val(0);
    $('#product_edit_form #type_edit').val('increase');
    $('#product_edit_form #current_product').val(product_id);


    $('#product_edit_modal').modal();
}


function addProduct(serialized_data){
    // console.log(serialized_data);
    var is_success = false;
    $("#loading_overlay1").show();
    $.ajax(
        {
            type: "POST",
            url: "/products/add-product",
            data: serialized_data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                $("#loading_overlay1").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay1").fadeOut(300);
                is_success = false;
            },
            success: function(data){
                if(data.result == "success"){
                    is_success = true;
                }else if(data.result == "error"){
                    is_success = false;
                    alert(data.message);
                }
                $("#loading_overlay1").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay1").fadeOut(300);
                    is_success = false;
                }
            }
        });
    return is_success;
}

function editProduct(serialized_data){
    // console.log(serialized_data);
    var is_success = false;
    $("#loading_overlay2").show();
    $.ajax(
        {
            type: "POST",
            url: "/products/edit-product",
            data: serialized_data,
            contentType: "application/x-www-form-urlencoded;",
            dataType: "json",
            async: false,
            beforeSend: function() {
                $("#loading_overlay2").fadeIn(300);
            },
            error: function(data, status, errThrown){
                $("#loading_overlay2").fadeOut(300);
                is_success = false;
            },
            success: function(data){
                if(data.result == "success"){
                    is_success = true;
                }else if(data.result == "error"){
                    is_success = false;
                    alert(data.message);
                }
                $("#loading_overlay2").fadeOut(300);
            },
            statusCode: {
                404: function() {
                    alert('page not found');
                    $("#loading_overlay2").fadeOut(300);
                    is_success = false;
                }
            }
        });
    return is_success;
}


$("#file-input-csv").on("change",function() {
    var file = $(this).prop("files")[0];
    var ext = file.name.split(".").pop().toLowerCase();
    if($.inArray(ext, ["csv"]) == -1) {
        $("#file-input-csv").val(null);
        alert('Upload CSV');
        return false;
    }

    $("#loading_overlay").show();
    var formData = new FormData();
    formData.append("uploadFile", file);
    $("#file-input-csv").val(null);
    $.ajax({
        async : false,
        xhr : function() {
            XHR = $.ajaxSettings.xhr();
            if (XHR.upload) {
                XHR.upload.addEventListener('progress', function(e) {
                }, false);
            }
            return XHR;
        },
        url: "/products/upload",
        method : "POST",
        data : formData,
        dataType : "json",
        contentType : false,
        processData : false,
        maxTimeout : 4000,
        beforeSend: function() {
            $("#loading_overlay").fadeIn(300);
        },
        error: function(data, status, errThrown){
            $("#loading_overlay").fadeOut(300);
        },
        success : function(data) {
            bootbox.alert(data.msg);
            make_init_data();
            $("#loading_overlay").fadeOut(300);
        },
        complete : function() {


        },
        statusCode: {
            404: function() {
                alert('page not found');
                $("#loading_overlay").hide();
            }
        }
    });
});