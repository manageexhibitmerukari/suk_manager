/**
 * Created by quankim on 12/04/2016.
 */
$('#change_password_form').formValidation({
    framework: 'bootstrap',
    excluded: [':disabled'],
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    err: {
        // You can set it to popover
        // The message then will be shown in Bootstrap popover
        container: 'tooltip'
    },
    fields: {
         current_password: {
            verbose: false,
            validators: {
                notEmpty: {
                    message: '現在のパスワードが必要です。'
                }
            }
        },
        password1: {
            validators: {
                notEmpty: {
                    message: '新しいパスワードが必要です。'
                }
            }
        },
        password2: {
            validators: {
                notEmpty: {
                    message: '新しいパスワードが必要です。'
                },
                callback: {
                    message: 'パスワードの確認が違います。',
                    callback: function (value, validator, $field) {
                        var item_value = $('#password1').val();
                        return value == item_value;
                    }
                }
            }
        }
    }
}).on('success.form.fv', function (e) {
    // Prevent form submission
    //e.preventDefault();

    // Retrieve instances
    //var $form = $(e.target),        // The form instance
    //   fv = $(e.target).data('formValidation'); // FormValidation instance

    // Do whatever you want here ... ajax request
});