<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 3/8/17
 * Time: 19:20
 */

namespace App\Controller;

use Cake\Utility\Security;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public function login ()
    {
        if ($this->Auth->user()){

            $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        }

        $error  = null;
        if ($this->request->is('post')) {

            $user = $this->Auth->identify();

            if ($user)
            {
                $this->Auth->setUser($user);
                $this->redirect($this->Auth->redirectUrl());
            } else {
                $error = 'ユーザーIDまたはパスワードが正しくありません。';
            }
        }

        $this->set(compact('error'));
    }

    public function changePassword()
    {
        $result = null;
        $message = null;

        if ($this->request->is('post'))
        {
            $currentPassword = $this->request->getData('current_password');
            $password1 = $this->request->getData('password1');

            $user = $this->Users->get($this->Auth->user('id'));

            $currentPassword = Security::hash($currentPassword, 'md5', true);

            if ($currentPassword == $user->password)
            {
                $user->password = $password1;
                $this->Users->save($user);

                $result = true;
                $message = 'パスワードを変更しました。';
            }
            else
            {
                $result = false;
                $message = '現在のパスワードが正しくありません。';
            }
        }

        $this->set(compact('result', 'message'));
    }

    public function logout()
    {
        $this->request->session()->destroy();
        $this->Auth->logout();
        return $this->redirect(['action' => 'login']);
    }
}