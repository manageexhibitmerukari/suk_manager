<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 3/9/17
 * Time: 10:10
 */

namespace App\Controller;
use ParseCSV\parseCSV;

/**
 *
 * @property \App\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController
{
    public function index() {
        if($this->request->is('ajax')) {
            $products = $this->Products->find('all', ['conditions' => ['user_id =' => $this->Auth->user('id')]]);

            $this->set(compact('products'));
            $this->set('_serialize', ['products']);
        }
    }

    public function removeCheckedProducts()
    {
        if($this->request->is('ajax'))
        {
            $itemIds = $this->request->getData('product_id_array');
            $itemIds = json_decode($itemIds);


            foreach ( $itemIds as $itemId)
            {
                $productInfo = $this->Products->get($itemId);

                $this->Products->delete($productInfo);
            }


            $result = 'success';

            $this->set(compact('result'));
            $this->set('_serialize', ['result']);
        } else {
            $this->redirect(['action' => 'index']);
        }
    }

    public function addProduct()
    {
        if($this->request->is('ajax'))
        {
            $suk = $this->request->getData('suk');

            $result = 'error';
            $message = '';

            if ($this->Products->exists(['suk' => $suk, 'user_id' => $this->Auth->user('id')]))
            {
                $message = 'SUK has been used!';
            }
            else
            {
                $product = $this->Products->newEntity([
                    'suk' => $suk,
                    'note' => $this->request->getData('note'),
                    'remain_amount' => $this->request->getData('remain_amount'),
                    'user_id' => $this->Auth->user('id'),
                    'price' => $this->request->getData('price'),
                ]);

                $this->Products->save($product);

                $result = 'success';
            }



            $this->set(compact('result', 'message'));
            $this->set('_serialize', ['result', 'message']);
        } else {
            $this->redirect(['action' => 'index']);
        }
    }

    public function editProduct()
    {
        if($this->request->is('ajax'))
        {
            $currentProduct = $this->request->getData('current_product');
            $suk = $this->request->getData('suk');
            $remainAmount = $this->request->getData('remain_amount');
            $typeEdit = $this->request->getData('type_edit');
            $price = $this->request->getData('price');

            $result = 'error';
            $message = '';

            if ($this->Products->exists(['id is not' => $currentProduct, 'suk' => $suk, 'user_id' => $this->Auth->user('id')]))
            {
                $message = 'SUK has been used!';
            }
            else
            {
                $updateResult = $this->Products->updateRecode($currentProduct, [
                    'suk' => $suk,
                    'note' => $this->request->getData('note'),
                    'remain_amount' => $remainAmount,
                    'price' => $price,
                ], $typeEdit);

                if($updateResult)
                    $result = 'success';
                else
                    $message = 'Update failed. Please try again!';
            }


            $this->set(compact('result', 'message'));
            $this->set('_serialize', ['result', 'message']);
        } else {
            $this->redirect(['action' => 'index']);
        }
    }

    public function upload()
    {
        $sukKey = 'SUK';
        $sukKey2 = '管理番号';
        $remainAmountKey = '在庫';
        $noteKey = '備考';
        $priceKey = '価格';

        $target_dir = WWW_ROOT."files/upload/".$this->Auth->user('id')."/";
        if (!is_dir($target_dir)) {
            mkdir($target_dir, 0777,true);
        }
        $file = $this->request->getData('uploadFile');
        $target_file = $target_dir . $file['name'];
        $type = pathinfo($target_file,PATHINFO_EXTENSION);
        if(strtolower($type) != 'csv'){
            exit;
        }
        $inputFile = $file['tmp_name'];
        $encodeFile = $target_dir.'encode.txt';
        shell_exec('nkf -w "'.$inputFile.'" > "'.$encodeFile . '"');
        $csv = new parseCSV();

        $csv->delimiter = $csv->auto($encodeFile);

        $csv->parse($encodeFile);
        $products = $csv->data;
        if($products)
        {
            foreach ($products as $key => $product)
            {
                if(array_key_exists($sukKey, $product))
                    $suk = $product[$sukKey];
                else
                    $suk = $product[$sukKey2];

                $remainAmount = $product[$remainAmountKey];
                $note = $product[$noteKey];
                $price = $product[$priceKey];

                if( $suk == '' || $remainAmount == '' || !is_numeric($remainAmount)) continue;

                if ($this->Products->exists(['suk' => $suk, 'user_id' => $this->Auth->user('id')]))
                {

                }
                else
                {
                    $product = $this->Products->newEntity([
                        'suk' => $suk,
                        'note' => $note,
                        'price' => $price,
                        'remain_amount' => $remainAmount,
                        'user_id' => $this->Auth->user('id'),
                    ]);

                    $this->Products->save($product);
                }
            }
            $msg = "アップロードできました。";
        }
        else
        {
            $msg = ".csvファイルエンコードに問題がありますので、ファイルを開くことができません。";
        }

        $this->set(compact('msg'));
        $this->set('_serialize', ['msg']);
    }
}