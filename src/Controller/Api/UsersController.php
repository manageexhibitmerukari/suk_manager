<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 3/8/17
 * Time: 16:08
 */

namespace App\Controller\Api;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */

class UsersController extends AppController
{
    public function register()
    {
        $username = $this->request->getData('username');
        $password = $this->request->getData('password');

        $result = "error";
        $data = null;
        $message = "error";

        if ($this->Users->exists(['username' => $username]))
        {
            $message = "登録済みのログインIDです。";
        }
        else
        {
            $user = $this->Users->newEntity([
                'username' => $username,
                'password' => $password
            ]);
            if ($this->Users->save($user))
            {
                $result = 'OK';
            }
            else
            {
                $message = 'The user could not be saved. Please, try again.';
            }
        }

        $this->returnResponse($result, $data, $message);
    }
}