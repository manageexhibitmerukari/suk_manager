<?php
/**
 * Created by PhpStorm.
 * User: nguyenthanhson
 * Date: 3/8/17
 * Time: 16:59
 */

namespace App\Controller\Api;

use Cake\I18n\Time;
/**
 * Users Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController
{
    public function getProducts()
    {
        $result = 'error';
        $data = null;
        $message = "";


        $user = $this->Auth->identify();
        if ($user)
        {
            $result = 'OK';
            $products = $this->Products->find('all', ['conditions' => ['user_id' => $user['id']]]);

            $data = [];
            foreach ($products as $product)
            {
                $data[] = [
                    'SUK' => $product->suk,
                    'note' => $product->note,
                    'remain_amount' => $product->remain_amount,
                    'price' => $product->price,
                    'created' => $product->created->getTimestamp(),
                    'modified' => $product->modified->getTimestamp()
                ];
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function getProduct()
    {
        $result = 'error';
        $data = null;
        $message = "";


        $user = $this->Auth->identify();
        if ($user)
        {
            $suk = $this->request->getData('SUK');
            $product = $this->Products->find('all', ['conditions' => ['user_id' => $user['id'], 'suk' => $suk]])->first();

            if ($product)
            {
                $result = 'OK';

                $data['product'] = [
                    'SUK' => $product->suk,
                    'note' => $product->note,
                    'remain_amount' => $product->remain_amount,
                    'price' => $product->price,
                    'created' => $product->created->getTimestamp(),
                    'modified' => $product->modified->getTimestamp()
                ];
            }
            else
            {
                $message = 'Product not found';
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function getOutOfStockProducts()
    {
        $result = 'error';
        $data = null;
        $message = "";


        $user = $this->Auth->identify();
        if ($user)
        {
            $result = 'OK';
            $lastTime = date("Y-m-d H:i:s", $this->request->getData('last_time'));

            $conditions = [
                'user_id' => $user['id'],
                'remain_amount =' => 0,
            ];
            if (is_null($lastTime))
                $conditions['modified >='] = new \DateTime($lastTime);

            $products = $this->Products->find('all', [
                'conditions' => $conditions
            ]);

            $data = [];
            foreach ($products as $product)
            {
                $data[] = [
                    'SUK' => $product->suk,
                    'note' => $product->note,
                    'remain_amount' => $product->remain_amount,
                    'price' => $product->price,
                    'created' => $product->created->getTimestamp(),
                    'modified' => $product->modified->getTimestamp()
                ];

                $product->modified = Time::now();
                $this->Products->save($product);
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function reduceAmount()
    {
        $result = 'error';
        $data = null;
        $message = "";


        $user = $this->Auth->identify();
        if ($user)
        {
            $suk = $this->request->getData('SUK');
            $amount = $this->request->getData('amount');
            if(is_null($amount))
                $amount = 1;

            if(empty($suk) || !$this->Products->exists(['suk' => $suk, 'user_id' => $user['id']]))
            {
                $message = 'product not found';
            }
            else
            {
                $product = $this->Products->find()->where(['suk' => $suk, 'user_id' => $user['id']])->first();

                if ($product->remain_amount >= $amount)
                {
                    $updateResult = $this->Products->updateRecode($product->id, ['remain_amount' => $amount], 'reduce');

                    if($updateResult)
                    {
                        $result = 'OK';

                        $product = $this->Products->get($product->id);

                        $data['product'] = [
                            'SUK' => $product->suk,
                            'note' => $product->note,
                            'remain_amount' => $product->remain_amount,
                            'price' => $product->price,
                            'created' => $product->created->getTimestamp(),
                            'modified' => $product->modified->getTimestamp()
                        ];
                    }
                    else
                        $message = 'Update failed. Please try again!';
                }
                else
                {
                    $message = 'remain amount less than ' . $amount;
                }
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function increaseAmount() {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        $products = $this->request->getData('products');

        if ($user)
        {
            $result = 'OK';

            if ($products) {
                foreach ($products as $row) {
                    if ($this->Products->exists(['suk' => $row['sku'], 'user_id' => $user['id']])) {

                        $product = $this->Products->find()->where(['suk' => $row['sku'], 'user_id' => $user['id']])->first();
                        $product->remain_amount = $product['remain_amount'] + $row['amount'];
                        $this->Products->save($product);

                    } else {

                        $product = $this->Products->newEntity();
                        $product = $this->Products->patchEntity($product, [
                            'suk' => $row['sku'],
                            'user_id' => $user['id'],
                            'note' => '',
                            'remain_amount' => $row['amount'],
                            'price' => 0,
                        ]);

                        $this->Products->save($product);
                    }
                }
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }

    public function increaseAmountApp() {
        $result = 'error';
        $data = null;
        $message = "";

        $user = $this->Auth->identify();

        $products = $this->request->getData('products');

        if ($user)
        {
            $result = 'OK';

            foreach ($products as $row) {
                if ($this->Products->exists(['suk' => $row['sku'], 'user_id' => $user['id']])) {

                    $product = $this->Products->find()->where(['suk' => $row['sku'], 'user_id' => $user['id']])->first();
                    $product->remain_amount = $product['remain_amount'] + $row['amount'];
                    $this->Products->save($product);

                } else {
                    $result = 'error';
                    $message = "倉庫にはこの商品" .$row['sku']. "が存在していません。";
                    break;
                }
            }
        }
        else
        {
            $message = "ユーザーIDまたはパスワードが正しくありません。";
        }

        $this->request->session()->destroy();
        $this->Auth->logout();

        $this->returnResponse($result, $data, $message);
    }
}