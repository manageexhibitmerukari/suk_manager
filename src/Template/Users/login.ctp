<?= $this->Html->script('site/login.js', ['block' => 'script']) ?>
<section class="content">
	<div class="login-box">
	    <!-- BEGIN Login Form -->
	    <div class="login-box-body">
		    <div class="login-logo">
		        <a href="javascript:void(0)"><?php echo "ログイン"; ?></a>
		    </div><!-- /.login-logo -->

		    <?= $this->Flash->render('auth') ?>
			<?= $this->Form->create() ?>
				<!-- error message -->
				<div class="form-group has-feedback">
		            <?php
			            if(!is_null($error)){
			                echo '<div class="alert alert-error">';
			                    echo '<p style="text-align:center;">'.$error.'</p>';
			                echo '</div>';
			            }
		            ?>
		            <!-- END error message -->

		            <div class="form-group has-feedback">
			            <div class="input-group_nouse">
			                <?= $this->Form->input('username', ['label' => false, 'class' => 'form-control', 'placeholder' => 'ユーザーIDを入力してください。']) ?>	
			            </div>
			            <span class="glyphicon glyphicon-user form-control-feedback"></span>
			        </div>

			        <div class="form-group has-feedback">
			            <div class="input-group_nouse">
			                <?= $this->Form->input('password', ['label' => false, 'class' => 'form-control', 'placeholder' => 'パスワードを入力してください。']) ?>
			            </div>
			            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
			        </div>

					<div class="row" style="margin:0px;">
			            <div class="col-xs-5 fix-format" style ="width:100%">
			                <button type="submit" onclick="return check_data();" name="submit" class="btn btn-primary btn-block btn-flat">
			                    ログイン&nbsp;<i class="fa fa-sign-in"></i>
			                </button>
			            </div>
			        </div>
			    </div>
			<?= $this->Form->end() ?>
		    </form>
		</div>

		<style>
		    .input-group-addon{font-size: 12px;}
		    .form-control{font-size: 12px;}
		    .btn{font-size: 12px}
		    .login-logo, .register-logo{font-size:30px;}
			.pull-right{float: right;}
		</style>
	    <!-- END Login Form -->
	</div><!-- /.login-box -->
</section>