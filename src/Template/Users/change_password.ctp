<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/change_password.js', ['block' => 'script']) ?>

    <section class="content-header">
        <h1>
            パスワード変更
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i>ホーム</li>
            <li class="active"><i class="glyphicon glyphicon-lock"></i> <b>パスワード変更</b></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" id="list_container">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-lock"></i>
                        <h3 class="box-title">パスワード変更</h3>
                    </div>

                    <div class="box-body table-responsive  with-border" id="main_tbl_wrap">
                        <div class="alert alert-warning col-xs-7" style="float: none; margin: 0 auto 20px; display: none;">

                        </div>

                        <div class="alert alert-info col-xs-7" style="float: none; margin: 0 auto 20px; display: none;">
                            <p style="font-size: 16px;">
                                パスワードを変更しました。
                            </p>
                        </div>

                        <?php
                        if(isset($result))
                        {
                            if ($result)
                            {
                                echo '<div class="alert alert-success col-xs-7" style="float: none; margin: 0 auto 20px;">';
                                echo '<p style="text-align:center; font-size: 16px;">'.$message.'</p>';
                                echo '</div>';
                            }
                            else
                            {
                                echo '<div class="alert alert-error col-xs-7" style="float: none; margin: 0 auto 20px;">';
                                echo '<p style="text-align:center; font-size: 16px;">'.$message.'</p>';
                                echo '</div>';
                            }
                        }
                        ?>


                        <?= $this->Form->create('Users', ['class' => 'form-horizontal col-xs-11', 'id' => 'change_password_form']) ?>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">現在のパスワード</label>
                                <?= $this->Form->input('current_password', [
                                    'templates' => [
                                        'inputContainer' => '<div class="col-xs-7">{{content}}</div>'
                                    ],
                                    'label' => false,
                                    'class' => 'form-control input-sm',
                                    'type' => 'password'
                                ]) ?>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 control-label">新しいパスワード</label>
                                <?= $this->Form->input('password1', [
                                    'templates' => [
                                        'inputContainer' => '<div class="col-xs-7">{{content}}</div>'
                                    ],
                                    'label' => false,
                                    'class' => 'form-control input-sm',
                                    'type' => 'password'
                                ]) ?>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 control-label">新しいパスワード（確認）</label>
                                <?= $this->Form->input('password2', [
                                    'templates' => [
                                        'inputContainer' => '<div class="col-xs-7">{{content}}</div>'
                                    ],
                                    'label' => false,
                                    'class' => 'form-control input-sm',
                                    'type' => 'password'
                                ]) ?>
                            </div>
                            <div class="form-group clearfix">
                                <button type="submit"  id="change_record" name="change_record" style="margin:0 auto; display:block;" class="btn btn-success ladda-button pull-center"><i class="fa fa-save"></i> セーブ</button>
                            </div>
                        <?= $this->Form->end() ?>
                    </div>

                    <div id="loading_overlay" class="overlay" style="display: none;">
                        <!--                        <img class="fa" src="./images/lib/ajax-loader1.gif">-->
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                </div>
            </div>
        </div>
    </section>
