<?= $this->extend('/Layout/main') ?>
<?= $this->Html->script('site/product_list.js', ['block' => 'script']) ?>


<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        出品管理
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>ホーム</li>
        <li class="active"><i class="glyphicon glyphicon-list"></i> <b>在庫管理</b></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" id="list_container">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title">在庫管理</h3>
                    <a href="/files/download/products.csv" class="btn btn-success btn-sm">
                        テンプレートダウンロード
                    </a>

                    <input id="file-input-csv" style="display: none;" accept=".csv" type="file">
                    <button type="button" class="btn btn-primary btn-sm" style="margin-left: 15px;" onclick="$('#file-input-csv').click()">
                        アップロード
                    </button>



                    <button onclick="displayInsertUI();" class="btn btn-success btn-sm pull-right">
                        <i class="fa fa-plus"></i> 商品登録
                    </button>
                </div>

                <form action="#" onsubmit="return false" enctype="multipart/form-data" role="form" name="product_main" id="product_main" class="form-horizontal">
                    <div class="box-body table-responsive  with-border" id="main_tbl_wrap">
                    </div>
                </form>
                <div class="box-footer text-right">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="button" onclick="RemoveCheckedProduct()" class="btn btn-danger btn-sm pull-left"  style="margin-left: 15px" data-style="zoom-in" >
                                削除
                            </button>
                        </div>
                    </div>
                </div>

                <div id="loading_overlay" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="product_add_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create('Products', ['url' => 'add', 'id' =>'product_add_form', 'class' => 'form-horizontal' ]) ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i>  商品登録</h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="suk">SUK</label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="suk" id="suk"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="remain_amount">在庫</label>
                    <div class="col-xs-7">
                        <input type="number" class="form-control input-sm" name="remain_amount" id="remain_amount" min="0" value="0"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="price">価格</label>
                    <div class="col-xs-7">
                        <input type="number" class="form-control input-sm" name="price" id="price" min="0"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="note">備考</label>
                    <div class="col-xs-7">
                        <textarea type="text" class="form-control input-sm" name="note" id="note"></textarea>
                    </div>
                </div>
            </div>

            <div class="modal-footer clearfix">
                <button type="submit"  id="change_record" name="change_record" data-style="zoom-in" class="btn btn-success ladda-button pull-center"><i class="fa fa-save"></i> セーブ</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> キャンセル</button>
            </div>
            <?= $this->Form->end() ?>

            <div class="overlay-wrapper">
                <div id="loading_overlay2" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="product_edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create('Products', ['url' => 'edit', 'id' =>'product_edit_form', 'class' => 'form-horizontal' ]) ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-edit"></i>  在庫変更</h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label class="col-xs-4 control-label" for="suk_edit">SUK</label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control input-sm" name="suk" id="suk_edit"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-xs-4 control-label" for="remain_amount_edit">在庫</label>
                    <div class="col-xs-7">
                        <input type="number" class="form-control input-sm" name="remain_amount" id="remain_amount_edit" min="0" value="0"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-xs-4 control-label" for="type_edit"></label>
                    <div class="col-xs-7">
                        <select name="type_edit" id="type_edit">
                            <option value="increase">加算</option>
                            <option value="reduce">減算</option>
                            <option value="specific">実数</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-xs-4 control-label" for="price_edit">価格</label>
                    <div class="col-xs-7">
                        <input type="number" class="form-control input-sm" name="price" id="price_edit" min="0"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-xs-4 control-label" for="note_edit">備考</label>
                    <div class="col-xs-7">
                        <textarea type="text" class="form-control input-sm" name="note" id="note_edit"></textarea>
                    </div>
                </div>
            </div>

            <div class="modal-footer clearfix">
                <button type="submit"  id="change_record" name="change_record" data-style="zoom-in" class="btn btn-success ladda-button pull-center"><i class="fa fa-save"></i> セーブ</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> キャンセル</button>
                <input type="hidden" id="current_product" value="0" name="current_product" />
            </div>
            <?= $this->Form->end() ?>

            <div class="overlay-wrapper">
                <div id="loading_overlay2" class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
</div>