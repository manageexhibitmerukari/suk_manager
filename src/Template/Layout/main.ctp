<?= $this->start('header') ?>
    <header class="main-header">
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>M</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
                <b>在庫一括管理</b>
            </span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a class="sidebar-toggle" data-toggle="offcanvas" role="button"></a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#">
                            <?php echo $currentUser->username; ?>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
<?= $this->end() ?>

<?= $this->start('aside') ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">&nbsp;</li>

                <!--<li>
                    <a href="javascript:void(0)">
                        <i class="fa fa-shopping-cart"></i>&nbsp;<span>出品管理</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                    </ul>
                </li>-->
                <li>
                    <?= $this->Html->link(
                        '&nbsp;&nbsp;<i class="fa fa-list"></i>&nbsp;<span>在庫管理</span>',
                        ['controller' => 'products', 'action' => 'index'],
                        ['escapeTitle' => false]) ?>
                </li>


                <li>
                    <?= $this->Html->link(
                        '<i class="fa fa-lock" aria-hidden="true"></i>&nbsp;<span>パスワード変更</span>',
                        ['controller' => 'users', 'action' => 'changePassword'],
                        ['escapeTitle' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link(
                    '<i class="fa fa-power-off"></i>&nbsp;<span>ログアウト</span>',
                    ['controller' => 'users', 'action' => 'logout'],
                    ['escapeTitle' => false]) ?>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
<?= $this->end() ?>

<!-- Main content -->

    <div class="content-wrapper">
        <?= $this->fetch('content') ?>
    </div>

<?= $this->start('footer') ?>
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>バージョン</b> 1.0
        </div>
    </footer>
<?= $this->end() ?>