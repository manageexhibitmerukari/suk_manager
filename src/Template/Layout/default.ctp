<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = '在庫一括管理';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>


    <!-- <?= $this->Html->css('base.css') ?> -->
    <!-- <?= $this->Html->css('cake.css') ?> -->
    <?= $this->Html->css('skin/bootstrap/css/bootstrap.min.css') ?>

    <?= $this->Html->css('skin/bootstrap/css/bootstrap-switch.css') ?>

    <?= $this->Html->css('font-awesome.min.css') ?>

    <?= $this->Html->css('ionicons.min.css') ?>

    <?= $this->Html->css('plugins/daterangepicker/daterangepicker-bs3.css') ?>

    <?= $this->Html->css('plugins/datepicker/datepicker3.css') ?>

    <?= $this->Html->css('plugins/iCheck/all.css') ?>

    <?= $this->Html->css('plugins/colorpicker/bootstrap-colorpicker.min.css') ?>

    <?= $this->Html->css('plugins/timepicker/bootstrap-timepicker.min.css') ?>

    <?= $this->Html->css('skin/dist/css/AdminLTE.css') ?>

    <?= $this->Html->css('skin/dist/css/custom.css') ?>

    <?= $this->Html->css('skin/dist/css/skins/_all-skins.css') ?>

    <?= $this->Html->css('plugins/datatables/dataTables.bootstrap.css') ?>

    <?= $this->Html->css('plugins/validation/formValidation.min.css') ?>

    <?= $this->Html->css('plugins/jquery-autocomplete/jquery.autocomplete.css') ?>

    <!--<?= $this->Html->css('plugins/select2/select2.css') ?>-->
    <?= $this->Html->css('plugins/select2/select2.min.css') ?>

    <!--<?= $this->Html->css('plugins/select2/select2-bootstrap.css') ?>-->

    <?= $this->Html->css('plugins/ladda/ladda-themeless.min.css') ?>

    <?= $this->Html->css('customize-styles.css') ?>
    <!--<?= $this->Html->css('furiru.css') ?>-->
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <style>
        html, body {
            height: 100%;
        }
        .color-palette {
            height: 35px;
            line-height: 35px;
            text-align: center;

        }
        .color-palette-set {
            margin-bottom: 15px;
        }
        .color-palette span {
            display: none;
            font-size: 12px;
        }
        .color-palette:hover span {
            display: block;
        }
        .color-palette-box h4 {
            position: absolute;
            top: 100%;
            left: 25px;
            margin-top: -40px;
            color: rgba(255, 255, 255, 0.8);
            font-size: 12px;
            display: block;
            z-index: 7;
        }
    </style>
</head>
<body class="skin-blue sidebar-mini wysihtml5-supported">
    <div class="wrapper">
        <?= $this->fetch('header') ?>

        <?= $this->fetch('aside') ?>

        <?= $this->fetch('content') ?>

        <?= $this->fetch('footer') ?>



        <!--<?= $this->html->script('plugins/jQuery/jQuery-2.1.4.min.js') ?>-->
        <?= $this->html->script('plugins/jQuery/jquery-2.2.1.min.js') ?>

        <?= $this->html->script('plugins/bootbox/bootbox.js') ?>

        <?= $this->html->script('skin/bootstrap/js/bootstrap.min.js') ?>

        <?= $this->html->script('skin/bootstrap/js/bootstrap-switch.js') ?>

        <?= $this->html->script('plugins/vuejs/vue.min.js') ?>

        <?= $this->html->script('plugins/input-mask/jquery.inputmask.js') ?>

        <?= $this->html->script('plugins/input-mask/jquery.inputmask.date.extensions.js') ?>

        <?= $this->html->script('plugins/input-mask/jquery.inputmask.extensions.js') ?>

        <?= $this->html->script('plugins/datepicker/bootstrap-datepicker.js') ?>

        <?= $this->html->script('plugins/datepicker/locales/bootstrap-datepicker.ja.js') ?>

        <?= $this->html->script('plugins/timepicker/bootstrap-timepicker.min.js') ?>

        <?= $this->html->script('plugins/fastclick/fastclick.min.js') ?>

        <?= $this->html->script('plugins/ladda/spin.min.js') ?>

        <?= $this->html->script('plugins/ladda/ladda.min.js') ?>

        <?= $this->html->script('plugins/datatables/jquery.dataTables.min.js') ?>

        <?= $this->html->script('plugins/datatables/dataTables.bootstrap.min.js') ?>

        <?= $this->html->script('plugins/validation/formValidation.min.js') ?>

        <?= $this->html->script('plugins/validation/framework/bootstrap.min.js') ?>

        <?= $this->html->script('plugins/validation/language/ja_JP.js') ?>

        <?= $this->html->script('plugins/moment/moment.js') ?>
        <?= $this->html->script('plugins/jquery-autocomplete/jquery.autocomplete.js') ?>

        <?= $this->html->script('plugins/iCheck/icheck.min.js') ?>

        <?= $this->html->script('plugins/select2/select2.full.js') ?>
        <!--<?= $this->html->script('plugins/select2/select2.min.js') ?>-->

        <?= $this->html->script('skin/dist/js/app.min.js') ?>

        <?= $this->html->script('site/common.js') ?>

        <?= $this->html->script('skin/dist/js/demo.js') ?>

        <?= $this->html->script('site/public.js') ?>

        <?= $this->fetch('script') ?>
    </div>
</body>
</html>
