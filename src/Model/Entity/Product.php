<?php
namespace App\Model\Entity;

use Cake\I18n\Time;
use Cake\ORM\Entity;
use RowLocker\LockableInterface;
use RowLocker\LockableTrait;
use RowLocker\LockingException;

/**
 * Product Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $suk
 * @property string $note
 * @property int $remain_amount
 * @property int $price
 * @property \Cake\I18n\Time $locked_time
 * @property int $locked_by
 * @property string $locked_session
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\User $user
 */
class Product extends Entity implements LockableInterface
{

//    use LockableTrait;
    protected static $lockTimeout = 15;


    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    public function lock($by = null, $session = null)
    {
        if ($this->isLocked()) {
            throw new LockingException('This entity is already locked');
        }

        $this->set('locked_time', Time::now());
        if ($by !== null) {
            $this->set('locked_by', $by);
        }

        if ($session !== null) {
            $this->set('locked_session', $session);
        }
    }

    /**
     * Unlocks the entity
     *
     * @return void
     */
    public function unlock()
    {
        $this->set([
            'locked_by' => null,
            'locked_session' => null,
            'locked_time' => null
        ]);
    }

    /**
     * Returns true if the entity is locked
     *
     * @return bool
     */
    public function isLocked()
    {
        $now = time();
        $locked = $this->get('locked_time');
        $locked = $locked ? $locked->getTimestamp() : 0;

        return $locked && abs($now - $locked) < static::getLockTimeout();
    }

    /**
     * Returns the user that locked the entity if any
     *
     * @return string|null
     */
    public function lockOwner()
    {
        return $this->get('locked_by');
    }

    /**
     * Returns the session that locked the entity if any
     *
     * @return string|null
     */
    public function lockSession()
    {
        return $this->get('locked_session');
    }

    /**
     * Sets the amount of seconds the lock can be valid
     *
     * @param int $seconds AMount of seconds
     * @return void
     */
    public static function setLockTimeout($seconds)
    {
        static::$lockTimeout = (int)$seconds;
    }

    /**
     * Returs the amout of seconds a lock is valid for
     *
     * @return int
     */
    public static function getLockTimeout()
    {
        return static::$lockTimeout;
    }
}
