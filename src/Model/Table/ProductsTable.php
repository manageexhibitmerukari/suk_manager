<?php
namespace App\Model\Table;

use App\Model\Entity\Product;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('RowLocker.RowLocker');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('suk', 'create')
            ->notEmpty('suk');

        $validator
            ->integer('remain_amount')
            ->requirePresence('remain_amount', 'create')
            ->notEmpty('remain_amount');

        $validator
            ->integer('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->dateTime('locked_time')
            ->allowEmpty('locked_time');

        $validator
            ->integer('locked_by')
            ->allowEmpty('locked_by');

        $validator
            ->allowEmpty('locked_session');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function updateRecode($productId, $data, $editType = null)
    {
        if (is_null($data)) return true;

        $startTime = time();

        while ((time() - $startTime) < 30)
        {
            $product = $this->get($productId);

            if ($product->isLocked())
            {
                sleep(1);
                continue;
            }
            $product->lock();
            $this->save($product);

            if (isset($data['suk']))
                $product->suk = $data['suk'];

            if (isset($data['note']))
                $product->note = $data['note'];

            if (isset($data['price']))
                $product->price = $data['price'];


            if (isset($data['remain_amount']) && $editType !== null)
            {
                if ($editType == 'increase')
                {
                    $product->remain_amount += $data['remain_amount'];
                }
                else if ($editType == 'reduce')
                {
                    if ($product->remain_amount > $data['remain_amount'])
                        $product->remain_amount -= $data['remain_amount'];
                    else
                        $product->remain_amount = 0;
                }
                else if ($editType == 'specific')
                {
                    $product->remain_amount = $data['remain_amount'];
                }
            }

            $product->unlock();
            $this->save($product);

            return true;
        }
        return false;
    }
}
